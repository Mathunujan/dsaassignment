package assmnt.dnapali;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class DNAPaliFind {

	/*
	 * static Queue<Character> nameQue; static Stack<Character> nameStack;
	 * 
	 * public static boolean checkPali(String word) {
	 * 
	 * nameQue = new LinkedList<>(); nameStack = new Stack<>();
	 * 
	 * char[] charWord = word.toCharArray(); for (int i = 0; i < charWord.length;
	 * i++) { nameQue.add(charWord[i]); nameStack.push(charWord[i]);
	 * 
	 * } for (int i = 0; i < charWord.length; i++) { if (nameQue.poll() !=
	 * nameStack.pop()) { return false;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return true;
	 * 
	 * }
	 */

	public static void CheckMethod() {
		Queue<Character> nameQue;
		Stack<Character> nameStack;

		char[] dnaBox = { 'A', 'B', 'C', 'B', 'A', 'D', 'B', 'A', 'B', 'D', 'D', 'B', 'A', 'B', 'D' };
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the lenght to find palindrome DNA: ");
		int paliLength = scan.nextInt();
		int breakUse = 0;
		char[] paliArray = new char[paliLength];
		int o = 0;

		for (int i = 0; i < dnaBox.length; i++) {

			// breakUse = dnaBox.length - (dnaBox.length % paliLength);
			breakUse = i + paliLength;
			nameQue = new LinkedList<>();
			nameStack = new Stack<>();

			if (breakUse <= dnaBox.length) {

				for (int j = i; j < i + (paliLength); j++) {
					nameStack.push(dnaBox[j]);
					nameQue.add(dnaBox[j]);
					// System.out.print(dnaBox[j]);

				}
				int index = 0;

				while (!nameQue.isEmpty() && !nameStack.isEmpty()) {

					char poll = nameQue.poll();
					char pop = nameStack.pop();

					if (poll == pop) {
						// System.out.print(pop);
						paliArray[index] = pop;
						index++;

					} else {
						break;
					}
				}
				if (index > 0) {
					printPaliArray(paliArray);
					System.out.print("    " + "element " + (i + 1) + " to " + (i + paliLength));
					System.out.println();
				}

				

			}

		}

	}

	private static void printPaliArray(char[] paliArray) {

		for (int i = 0; i < paliArray.length; i++) {

			System.out.print(paliArray[i]);

		}

	}

}
